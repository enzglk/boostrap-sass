# Bootstrap4-Gulp Starter

### Installing

```
npm install
```

### Task
To start gulp
```
npm run watch
```
To clean / delete dist directory
```
npm run clean
```

### Dependencies
* bootstrap
* browser-sync
* del
* @fortawesome/fontawesome-free
* gulp
* gulp-htmlclean
* gulp-imagemin
* gulp-newer
* gulp-nunjucks-render
* gulp-autoprefixer
* gulp-preprocess
* gulp-rename
* gulp-sass
* gulp-size
* gulp-sourcemaps
* jquery
* popper.js
