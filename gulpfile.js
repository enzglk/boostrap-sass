var
    autoprefixer = require('gulp-autoprefixer'),
    browsersync = require('browser-sync'),
    del = require('del'),
    gulp = require('gulp'),
    htmlclean = require('gulp-htmlclean'),
    imagemin = require('gulp-imagemin'),
    newer = require('gulp-newer'),
    nunjucksRender = require('gulp-nunjucks-render'),
    pkg = require('./package.json'),
    preprocess = require('gulp-preprocess'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    size = require('gulp-size'),
    sourcemaps = require('gulp-sourcemaps');


var
source = 'src/',
dest = 'dist/',
nodedir = 'node_modules/',

nunjucks = {
    watch: [source + 'pages/**/*', source + 'templates/**/*'],
};


images = {
    in: source + 'images/**/*.*',
    out: dest + 'images/'
};

css = {
    in: source + 'styles/styles.scss',
    watch: [source + 'styles/**/*'],
    out: dest + 'css/',
    sassOpts: {
        outputStyle: 'compressed',
        precision: 3,
        errLogToConsole: true
    },
    pleeeaseOpts: {
        autoprefixer: { browsers: ['last 2 versions', '> 2%'] },
        rem: ['16px'],
        pseudoElements: true,
        mqpacker: true
    }
};

twbs = {
    in: source + 'bootstrap4/bootstrap.scss',
    watch: [source + 'bootstrap4/**/*'],
    out: dest + 'vendor/bootstrap/css/',
    sassOpts: {
        includePaths: [nodedir + '/bootstrap/scss'],
        outputStyle: 'compressed',
        precision: 3,
        errLogToConsole: true
    }
};

twbsjs = {
    in: nodedir + 'bootstrap/dist/js/*.*',
    out: dest + 'vendor/bootstrap/js/'
};

jquery = {
    in: nodedir + 'jquery/dist/*.js',
    out: dest + 'vendor/jquery/'
};

popperjs = {
    in: nodedir + 'popper.js/dist/**/*.*',
    out: dest + 'vendor/popper.js/'
};

script = {
    in: source + 'scripts/script.js',
    out: dest + 'js/'
};

syncOpts = {
    server: {
        baseDir: dest,
        index: 'index.html'
    },
    open: true,
    notify: true
};



// ============== TASK ============== //
gulp.task('clean', function(){
    del([
        dest + '*'
    ]);
});

// manage images
gulp.task('images', function(){
    return gulp.src(images.in)
        .pipe(newer(images.out))
        .pipe(imagemin())
        .pipe(gulp.dest(images.out));
});

// Font-Awesome
facss = {
    in: nodedir + '@fortawesome/fontawesome-free/css/all*.*',
    out: dest + 'vendor/fontawesome/css/'
},

fajs = {
    in: nodedir + '@fortawesome/fontawesome-free/js/all*.*',
    out: dest + 'vendor/fontawesome/js/'
},

fafont = {
    in: nodedir + '@fortawesome/fontawesome-free/webfonts/**/*',
    out: dest + 'vendor/fontawesome/webfonts/'
},

//copy popperjs
gulp.task('popperjs', function() {
    return gulp.src(popperjs.in)
        .pipe(newer(popperjs.out))
        .pipe(gulp.dest(popperjs.out))
});

//copy jquery
gulp.task('jquery', function() {
    return gulp.src(jquery.in)
        .pipe(newer(jquery.out))
        .pipe(gulp.dest(jquery.out))
});

//compile bootstrap
gulp.task('twbs', function(){
    return gulp.src(twbs.in)
        .pipe(sourcemaps.init())
        .pipe(sass(twbs.sassOpts).on('error', sass.logError))
        .pipe(autoprefixer({ browsers: ['last 2 versions', '> 2%'] }))
        .pipe(rename('bootstrap.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(twbs.out))
});

//copy bootstrapjs
gulp.task('twbsjs', function() {
    return gulp.src(twbsjs.in)
        .pipe(newer(twbsjs.out))
        .pipe(gulp.dest(twbsjs.out))
});

// copy fontawesome
gulp.task('facss', function(){
    return gulp.src(facss.in)
        .pipe(newer(facss.out))
        .pipe(gulp.dest(facss.out));
});
gulp.task('fajs', function(){
    return gulp.src(fajs.in)
        .pipe(newer(fajs.out))
        .pipe(gulp.dest(fajs.out));
});
gulp.task('fafont', function(){
    return gulp.src(fafont.in)
        .pipe(newer(fafont.out))
        .pipe(gulp.dest(fafont.out));
});

//copy script
gulp.task('script', function() {
    return gulp.src(script.in)
        .pipe(newer(script.out))
        .pipe(gulp.dest(script.out))
});

// compile Sass
gulp.task('sass', function() {
    return gulp.src(css.in)
        .pipe(sourcemaps.init())
        .pipe(sass(css.sassOpts).on('error', sass.logError))
        .pipe(size({title: 'CSS in '}))
        .pipe(autoprefixer({ browsers: ['last 2 versions', '> 2%'] }))
        .pipe(size({title: 'CSS out '}))
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(css.out))
        .pipe(browsersync.reload({ stream: true }));
});

gulp.task('nunjucks', function() {
    // Gets .html and .nunjucks files in pages
    return gulp.src('src/pages/**/*.+(html|nunjucks)')
    // Renders template with nunjucks
    .pipe(nunjucksRender({
        path: ['src/templates']
      }))
    // .pipe(htmlclean()) // uncomment to minimize the html output
    // output files in app folder
    .pipe(gulp.dest('dist'))
  });

  // browser sync
  gulp.task('browsersync', function() {
      browsersync(syncOpts);
  });


// default task
gulp.task('default', [
            'nunjucks', 
            'images', 
            'script', 
            'popperjs',
            'fafont',
            'facss',
            'fajs',
            'twbs', 
            'twbsjs', 
            'jquery', 
            'sass', 
            'browsersync'
        ], function() {

        // html changes
        gulp.watch(nunjucks.watch, ['nunjucks', browsersync.reload]);

        // image changes
        gulp.watch(images.in, ['images']);

        // bootstrap changes
        gulp.watch(twbs.watch, ['twbs', browsersync.reload]);

        // sass changes
        gulp.watch(css.watch, ['sass', browsersync.reload]);


    });
